const express=require("express"),
		app=express(),
		crypto=require("crypto"),
		port=3000,
		cors=require("cors");

function sendEncryptedResponse(req, res)
{
	var	sourceBuf=Buffer.from(Buffer.from(decodeURIComponent(req.query.sourceBuf), "base64").toString("binary")),
		numBytesSourceBuf=sourceBuf.byteLength,
		destinationBuf1=new Array(),
		destinationBuf2=new Array(),
		r=0,
		c=0,
		d=0;

	for(var i=0; i<numBytesSourceBuf; i++)
	{
		r=crypto.randomBytes(1).readUInt8(0);

		c=sourceBuf.readUInt8(i)+r;

		if(c>255)
			d=c-255;
		else
			d=c;

		destinationBuf1[i]=r;
		destinationBuf2[i]=d;
	}

	var result=
		{
			destinationBuf1: encodeURIComponent(Buffer.from(destinationBuf1, "binary").toString("base64")),
			destinationBuf2: encodeURIComponent(Buffer.from(destinationBuf2, "binary").toString("base64"))
		};

	res.json(result);
}

function sendDecryptedResponse(req, res)
{
	var	sourceBuf1=Buffer.from(decodeURIComponent(req.query.sourceBuf1), "base64"),
		sourceBuf2=Buffer.from(decodeURIComponent(req.query.sourceBuf2), "base64"),
		numBytesSourceBuf1=sourceBuf1.byteLength,
		numBytesSourceBuf2=sourceBuf2.byteLength,
		numBytes=0,
		destinationBuf=new Array(),
		c=0,
		d=0,
		numBytes=0;

	if(numBytesSourceBuf1<=numBytesSourceBuf2)
		numBytes=numBytesSourceBuf1;
	else
		numBytes=numBytesSourceBuf2;

	for(var i=0; i<numBytes; i++)
	{
		c=sourceBuf2.readUInt8(i)-sourceBuf1.readUInt8(i);

		if(c<0)
			d=c+255;
		else
			d=c;

		destinationBuf[i]=d;
	}

	var result=
		{
			destinationBuf: encodeURIComponent(Buffer.from(destinationBuf, "binary").toString("base64")),
		};

	res.json(result);
}

app.use(cors());

app.get("/encrypt", (req, res) => sendEncryptedResponse(req, res));
app.get("/decrypt", (req, res) => sendDecryptedResponse(req, res));

app.listen(port, () => console.log(`OTP Encryption Server 1.0 by Julian Meinold running on port ${port}.\nCopyright 2020 by Julian Meinold. All rights reserved.`));
